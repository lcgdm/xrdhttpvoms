DESCRIPTION

  This package provides the VOMS security extractor plugin for XrdHTTP. Given
  a working setup of XrdHTTP, this library will be able to extract the VOMS
  information from the certificate presented by the client. The extracted
  information will be available for other plugins to apply authorization rules.

  XrdHTTP itself is a contribution of CERN IT-GT to the Xrootd project,
  in order to promote the usage of the HTTP protocol for certain tasks.
  
  The xrootd codebase cannot depend on external libraries like libvoms. For this
  reason we are providing this little package separately.

CONTACT

  Send and email to dpm-users-forum@cern.ch if you have questions
  regarding the use of this software. To submit patches or suggest improvements
  send an email to dpm-devel@cern.ch

AUTHORS

  Fabrizio Furano (fabrizio.furano@cern.ch)

LICENSE

  Copyright 2014 CERN IT-GT

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

